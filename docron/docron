#!/usr/bin/env bash
# Author: Robin Taylor http://gitlab.com/RobinTaylor
#   Based on work by Alexander Epstein https://github.com/alexanderepstein
currentVersion="0.0.1"
configuredClient=""
projectId="11572930"

## This function determines which http get tool the system has installed and returns an error if there isnt one
getConfiguredClient()
{
  if  command -v curl &>/dev/null; then
    configuredClient="curl"
  elif command -v wget &>/dev/null; then
    configuredClient="wget"
  elif command -v http &>/dev/null; then
    configuredClient="httpie"
  elif command -v fetch &>/dev/null; then
    configuredClient="fetch"
  else
    echo "Error: This tool reqires either curl, wget, httpie or fetch to be installed." >&2
    return 1
  fi
}

## Allows to call the users configured client without if statements everywhere
httpGet()
{
  case "$configuredClient" in
    curl)  curl -A curl -s "$@" ;;
    wget)  wget -qO- "$@" ;;
    httpie) http -b GET "$@" ;;
    fetch) fetch -q "$@" ;;
  esac
}

update()
{
  # Author: Robin Taylor Epstein https://gitlab.com/robintaylor, based on work by Alexander Epstein https://github.com/AlexanderEpstein
  # Update utility version 0.0.1
  # To test the tool enter in the defualt values that are in the examples for each variable
  repositoryName="Bash-Bits" #Name of repostiory to be updated ex. Sandman-Lite
  gitlabUserName="RobinTaylor" #username that hosts the repostiory ex. RobinTaylor
  nameOfInstallFile="install.sh" # change this if the installer file has a different name be sure to include file extension if there is one
  latestVersion=$(httpGet https:/gitlab.com/api/v4/projects/$projectId/repository/tags | grep -Eo '"name":.*?[^\\]",'| head -1 | grep -Eo "[0-9.]+" ) #always grabs the tag without the v option

  if [[ $currentVersion == "" || $repositoryName == "" || $gitlabUserName == "" || $nameOfInstallFile == "" ]]; then
    echo "Error: update utility has not been configured correctly." >&2
    exit 1
  elif [[ $latestVersion == "" ]]; then
    echo "Error: no active internet connection" >&2
    exit 1
  else
    if [[ "$latestVersion" != "$currentVersion" ]]; then
      echo "Version $latestVersion available"
      echo -n "Do you wish to update $repositoryName [Y/n]: "
      read -r answer
      if [[ "$answer" == [Yy] ]]; then
        cd ~ || { echo 'Update Failed'; exit 1; }
        if [[ -d  ~/$repositoryName ]]; then rm -r -f $repositoryName || { echo "Permissions Error: try running the update as sudo"; exit 1; } ; fi
        git clone "https://gitlab.com/$gitlabUserName/$repositoryName" || { echo "Couldn't download latest version"; exit 1; }
        cd $repositoryName || { echo 'Update Failed'; exit 1; }
        git checkout "v$latestVersion" 2> /dev/null || git checkout "$latestVersion" 2> /dev/null || echo "Couldn't git checkout to stable release, updating to latest commit."
        chmod a+x install.sh #this might be necessary in your case but wasnt in mine.
        ./$nameOfInstallFile "update" || exit 1
        cd ..
        rm -r -f $repositoryName || { echo "Permissions Error: update succesfull but cannot delete temp files located at ~/$repositoryName delete this directory with sudo"; exit 1; }
      else
        exit 1
      fi
    else
      echo "$repositoryName is already the latest version"
    fi
  fi
}

checkInternet()
{
  httpGet github.com > /dev/null 2>&1 || { echo "Error: no active internet connection" >&2; return 1; } # query github with a get request
}

usage()
{
  cat <<EOF
docron
Description: --Runs the daily, weekly, and monthly system cron jobs on a
   system that's likely to be shut down during the usual time of day when
   the system cron jobs would otherwise be scheduled to run.
Usage: docron [flags] or docron [flags] [arguments]
  -u  Update docron tool
  -h  Show the help
  -v  Get the tool version
Examples:
   sudo docron daily
   sudo docron weekly
EOF
}

docron()
{
    rootcron="/etc/crontab"   # This is going to vary significantly based on
                              # which version of Unix or Linux you've got.

    if [ $# -ne 1 ] ; then
      echo "Usage: $0 [daily|weekly|monthly]" >&2
      exit 1
    fi

    # If this script isn't being run by the administrator, fail out. In
    #   earlier scripts, you saw USER and LOGNAME being tested, but in this
    #   situation, we'll check the user ID value directly. Root = 0.

    if [ "$(id -u)" -ne 0 ] ; then
       # Or you can use $(whoami) != "root" here, as needed.
      echo "$0: Command must be run as 'root'" >&2
      exit 1
    fi

    # We assume that the root cron has entries for 'daily', 'weekly', and
    #   'monthly' jobs. If we can't find a match for the one specified, well,
    #   that's an error. But first, we'll try to get the command if there is
    #   a match (which is what we expect).

    job="$(awk "NF > 6 && /$1/ { for (i=7;i<=NF;i++) print \$i }" $rootcron)"

    if [ -z "$job" ] ; then   # No job? Weird. Okay, that's an error.
      echo "$0: Error: no $1 job found in $rootcron" >&2
      exit 1
    fi

    SHELL='which sh'          # To be consistent with cron's default

    eval $job               # We’ll exit once the job is finished.
}

while getopts "uvh" opt; do
  case "$opt" in
    \?) echo "Invalid option: -$OPTARG" >&2
        exit 1
        ;;
    h)  usage
        exit 0
        ;;
    v)  echo "Version $currentVersion"
        exit 0
        ;;
    u)  getConfiguredClient || exit 1
        checkInternet || exit 1
        update
        exit 0
        ;;
    :)  echo "Option -$OPTARG requires an argument." >&2
        exit 1
        ;;
  esac
done

# special set of first arguments that have a specific behavior across tools
if [[ $# == "0" ]]; then
  usage ## if calling the tool with no flags and args chances are you want to return usage
  exit 0
elif [[ $# == "1" ]]; then
  if [[ $1 == "update" ]]; then
    getConfiguredClient || exit 1
    checkInternet || exit 1
    update || exit 1
    exit 0
  elif [[ $1 == "help" ]]; then
    usage
    exit 0
  elif [[ $1 == "daily" ]] || [[ $1 == "weekly" ]] || [[ $1 == "montly" ]]; then
    docron $1
  else
    usage
  fi
fi

## The rest of the conditions and code would go here
## Make sure to use checkInternet at least once before any time httpGet will be called.
## Make sure to call getConfiguredClient at least once before ever calling checkInternet.